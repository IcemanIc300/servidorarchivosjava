package servidorarchivosandroid;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManipularArchivos {

    String rutaArchivo;
    int idOpcion;
    int idSession;
    File manipulado;
    BufferedReader lee, lee2;
    FileOutputStream fos;
    PrintWriter pw;

    public ManipularArchivos(String ruta, int opcion, int id) {
        this.rutaArchivo = ruta;
        this.idOpcion = opcion;
        this.idSession = id;
    }

    public String ManipularArchivo() {
        switch (idOpcion) {
            case 0:
                Manipulacion1();
                break;
            case 1:
                Manipulacion2();
                break;
            case 2:
                Manipulacion3();
                break;
        }
        return null;
    }

    private void Manipulacion1() {
        try {
            manipulado = new File("cliente"+idSession+"/manipulado.txt");
            fos = new FileOutputStream(manipulado);
            pw = new PrintWriter(fos, true);
            lee = new BufferedReader(new FileReader(rutaArchivo));
            String sCadenaInvertida;
            String volteado = lee.readLine();
            while (volteado != null) {
                sCadenaInvertida = "";
                for (int x = volteado.length() - 1; x >= 0; x--) {
                    sCadenaInvertida += volteado.charAt(x);
                }
                pw.println(sCadenaInvertida);
                volteado = lee.readLine();
            }
            lee.close();
            fos.close();
            pw.flush();
            pw.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void Manipulacion2() {
        try {
            manipulado = new File("cliente"+idSession+"/manipulado.txt");
            fos = new FileOutputStream(manipulado);

            pw = new PrintWriter(fos, true);
            lee = new BufferedReader(new FileReader(rutaArchivo));
            String volteado = lee.readLine();
            int cont = 0;
            while (volteado != null) {
                cont++;
                volteado = lee.readLine();
            }
            lee.close();

            for (int i = cont; i >= 0; i--) {
                lee = new BufferedReader(new FileReader(rutaArchivo));
                String linea = "";
                for (int o = 0; o < i; o++) {
                    linea = lee.readLine();
                }
                pw.println(linea);
                lee.close();
            }
            lee.close();
            fos.close();
            pw.flush();
            pw.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void Manipulacion3() {
        try {
            manipulado = new File("cliente"+idSession+"/semimanipula.txt");
            fos = new FileOutputStream(manipulado);
            pw = new PrintWriter(fos, true);
            lee = new BufferedReader(new FileReader(rutaArchivo));
            String volteado = lee.readLine();
            while (volteado != null) {
                String sCadenaInvertida = "";
                for (int x = volteado.length() - 1; x >= 0; x--) {
                    sCadenaInvertida = sCadenaInvertida + volteado.charAt(x);
                }
                pw.println(sCadenaInvertida);
                volteado = lee.readLine();
            }
            lee.close();
            fos.close();
            pw.flush();
            pw.close();

            lee2 = new BufferedReader(new FileReader("cliente"+idSession+"/semimanipula.txt"));
            String linea = lee2.readLine();
            while (linea != null) {
                linea = lee2.readLine();
            }
            lee2.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        try {
            File arch = new File("cliente"+idSession+"/manipulado.txt");
            fos = new FileOutputStream(arch);
            pw = new PrintWriter(fos, true);
            lee = new BufferedReader(new FileReader("cliente"+idSession+"/semimanipula.txt"));
            String volteado = lee.readLine();
            int cont = 0;
            while (volteado != null) {
                cont++;
                volteado = lee.readLine();
            }
            lee.close();

            for (int i = cont; i >= 0; i--) {
                lee = new BufferedReader(new FileReader("cliente"+idSession+"/semimanipula.txt"));
                String linea = "";
                for (int o = 0; o < i; o++) {
                    linea = lee.readLine();
                }
                pw.println(linea);
                lee.close();
            }
            lee.close();
            fos.close();
            pw.flush();
            pw.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}
