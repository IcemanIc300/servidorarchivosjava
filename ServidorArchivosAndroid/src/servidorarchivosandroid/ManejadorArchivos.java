package servidorarchivosandroid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class ManejadorArchivos extends Thread{
    
    private final Socket clienteIndividual;
    private BufferedReader delCliente;
    private PrintWriter alCliente;
    private final int idSession;
    
    String cadenaTexto = "";
    
    public ManejadorArchivos(Socket socket, int id){
        this.clienteIndividual = socket;
        this.idSession = id;
        try{
            delCliente = new BufferedReader(new InputStreamReader(clienteIndividual.getInputStream()));
            alCliente = new PrintWriter(new OutputStreamWriter(clienteIndividual.getOutputStream()),true);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    public void desconectar(){
        try{
            alCliente.close();
            delCliente.close();
            clienteIndividual.close();
            System.out.println("Termine peticion de: "+idSession);
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    @Override
    public void run(){
        try {
            String mensaje = delCliente.readLine();
            System.out.println("Llego: "+mensaje);
            int numLineas = Integer.parseInt(delCliente.readLine());
            System.out.println("Tamaño del Archivo: " +numLineas+ " lineas");
            int idOpcion = Integer.parseInt(delCliente.readLine());
            switch(idOpcion){
                case 0:
                    System.out.println("El cliente: " +idSession+ " quiere invertir el archivo");
                    for (int i = 0; i < numLineas; i++) {
                        cadenaTexto+= delCliente.readLine()+"\n";
                    }
                    break;
                case 1:
                    System.out.println("El cliente: " +idSession+ " quiere invertir las lineas");
                    for (int i = 0; i < numLineas; i++) {
                        cadenaTexto+= delCliente.readLine()+"\n";
                    }
                    break;                    
                case 2:
                    System.out.println("El cliente: " +idSession+ " quiere ambas opciones");
                    for (int i = 0; i < numLineas; i++) {
                        cadenaTexto+= delCliente.readLine()+"\n";
                    }
                    break;
            }
            
            System.out.println(cadenaTexto);
            //Creamos el archivo para manipularlo
            File crear = new File("cliente"+idSession);
            crear.mkdir();
            File archivo = new File("cliente"+idSession+"/archivo.txt");
            BufferedWriter bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(cadenaTexto);
            bw.close();
            
            //Creamos el objeto, manipulamos y retornamos al cliente
            ManipularArchivos manArch = new ManipularArchivos("cliente"+idSession+"/archivo.txt", idOpcion, idSession);
            manArch.ManipularArchivo();
            File manipulado = new File("cliente"+idSession+"/manipulado.txt");
            BufferedReader br = new BufferedReader(new FileReader(manipulado));
            String lineaProcesada = br.readLine();
            while (lineaProcesada != null) {
                alCliente.println(lineaProcesada);
                lineaProcesada = br.readLine();
            }
            alCliente.println("Respuesta a: "+idSession);
            crear.delete();
        } catch (IOException | NumberFormatException ex) {
            System.out.println("Error" + ex.getMessage());
        }
        desconectar();
    }
    
}
