package servidorarchivosandroid;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorArchivosAndroid {
    
    ServerSocket cliente;
    
    public static void main(String[] args) {
        try {
            ServerSocket servidor = new ServerSocket(5555);
            System.out.println("En espera en el puerto 5555");
            int idSesion = 0;
            while(true){
                try{
                    Socket cliente = servidor.accept();
                    System.out.println("\n");
                    System.out.println("Se conecto un cliente con id: "+idSesion);
                    ((ManejadorArchivos) new ManejadorArchivos(cliente, idSesion)).start();
                    idSesion++;
                }catch(Exception ex){
                    System.out.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
